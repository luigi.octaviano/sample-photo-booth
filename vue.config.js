module.exports = {
  runtimeCompiler: true,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/photo-booth-example/'
    : '/',
  chainWebpack: (config) => {
      config
          .plugin('html')
          .tap(args => {
              args[0].title = 'Virtual Photo Booth';
              args[0].meta = {viewport: 'width=device-width,initial-scale=1,user-scalable=no', 'theme-color': '#0274BE'};
  
           return args;
      })
  }
};
