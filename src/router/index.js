import Vue from "vue";
import Router from "vue-router";

import Home from '@/components/Home'
import Preview from '@/components/Preview'
import Finale from '@/components/Finale'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/preview',
      name: 'Preview',
      component: Preview,
      props: true
    },
    {
      path: '/final-preview',
      name: 'Finale',
      component: Finale,
      props: true
    },
  ]
});